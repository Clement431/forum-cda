<?php

include_once 'Repository/UserRepository.php';
include_once 'InterfaceController.php';
include_once 'Entity/User.php';

class UserController implements InterfaceController
{
    private $userRepository;

    public  function __construct(){
        $this->userRepository = new UserRepository();
    }

    public function show()
    {
        $users= $this->userRepository->findAll();
        echo json_encode($users);

    }

    public function add(){

        $dataUser = json_decode(file_get_contents('php://input'), true);
        $user = new User();
        $user->setEmail($dataUser['email']);
        $user->setPassword($dataUser['password']);
        $user->setBirthDate(new \DateTime ($dataUser["birthDate"]));
        $this->userRepository->addUser($user);

    }

    public function update(){

        $dataUser = json_decode(file_get_contents('php://input'), true);
        $user = new User();
        $user->setId($_GET['id']);
        $user->setEmail($dataUser['email']);
        $user->setPassword($dataUser['password']);
        $user->setBirthDate(new \DateTime ($dataUser["birthDate"]));
         
         $this->userRepository->update($user);
    }
    
    public function delete(){

        $this->userRepository->delete($_GET['id']);

    }

   
}