<?php

include_once 'Repository/CategoryRepository.php';
include_once 'InterfaceController.php';
include_once 'Entity/Category.php';

class CategoryController implements InterfaceController
{
    private $categoryRepository;

    public  function __construct(){
        $this->categoryRepository = new CategoryRepository();
    }

    public function show()
    {
        $users= $this->categoryRepository->findAll();
        echo json_encode($users);

    }

    public function add(){

        $dataCategory = json_decode(file_get_contents('php://input'), true);
        $category = new Category();
        $category->setLabel($dataCategory['label']);
        $this->categoryRepository->addCategory($category);

    }

    public function update(){

       
        $dataCategory = json_decode(file_get_contents('php://input'), true);
        $category = new Category();
        $category->setId($_GET['id']);
        $category->setLabel($dataCategory['label']);
         
         $this->categoryRepository->update($category);
    }
    
    public function delete(){

        $this->categoryRepository->delete($_GET['id']);

    }

   
}