<?php

include_once 'Repository/TopicRepository.php';
include_once 'Repository/UserRepository.php';
include_once 'InterfaceController.php';
include_once 'Repository/CategoryRepository.php';
include_once 'Entity/Topic.php';

class TopicController implements InterfaceController
{
    private $topicRepository;

    public  function __construct(){
        $this->topicRepository = new TopicRepository();
    }

    public function show()
    {
        $topic= $this->topicRepository->findAll();
        echo json_encode($topic);

    }

    public function add(){

        $dataTopic = json_decode(file_get_contents('php://input'), true);
        $topic = new Topic();
        $userRepository = new UserRepository();
        $categoryRepository = new CategoryRepository();

        $topic->setTitle($dataTopic['title']);
        $topic->setUser($userRepository->findById($dataTopic['id_user']));
        $topic->setCategory($categoryRepository->findById($dataTopic['id_category']));
        
        $this->topicRepository->addTopic($topic);

    }

    public function update(){

        $dataTopic = json_decode(file_get_contents('php://input'), true);
        $topic = new Topic();
        $userRepository = new UserRepository();
        $categoryRepository = new CategoryRepository();

        $topic->setId($_GET['id']);
        $topic->setTitle($dataTopic['title']);
        $topic->setUser($userRepository->findById($dataTopic['id_user']));
        $topic->setCategory($categoryRepository->findById($dataTopic['id_category']));
         
         $this->topicRepository->update($topic);
    }
    
    public function delete(){

        $this->topicRepository->delete($_GET['id']);

    }

   
}