<?php

include_once 'Repository/PostRepository.php';
include_once 'Repository/UserRepository.php';
include_once 'Repository/TopicRepository.php';
include_once 'InterfaceController.php';
include_once 'Entity/Category.php';
include_once 'Entity/Post.php';

class PostController implements InterfaceController
{
    private $postRepository;

    public  function __construct(){
        $this->postRepository = new PostRepository();
    }

    public function show()
    {
        $post= $this->postRepository->findAll();
        echo json_encode($post);

    }

    public function add(){

        $dataPost = json_decode(file_get_contents('php://input'), true);
        $userRepository = new UserRepository();
        $topicRepository = new TopicRepository();
        $post = new Post();

        $post->setPostDate(new \DateTime ($dataPost['post_date']));
        $post->setContent($dataPost['content']);
        $post->setUser($userRepository->findById($dataPost['id_user']));
        $post->setTopic($topicRepository->findById($dataPost['id_topic']));

        $this->postRepository->addPost($post);

    }

    public function update(){

       
        $dataPost = json_decode(file_get_contents('php://input'), true);
       
        $userRepository = new UserRepository();
        $topicRepository = new TopicRepository();
        $post = new Post();

        $post->setId($_GET['id']);
        $post->setPostDate(new \DateTime ($dataPost['post_date']));
        $post->setContent($dataPost['content']);
        $post->setUser($userRepository->findById($dataPost['id_user']));
        $post->setTopic($topicRepository->findById($dataPost['id_topic']));
         
         $this->postRepository->update($post);
    }
    
    public function delete(){

        $this->postRepository->delete($_GET['id']);

    }

   
}