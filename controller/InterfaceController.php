<?php

interface InterfaceController{

    public function show();
    public function add();
    public function update();
    public function delete();
    
}