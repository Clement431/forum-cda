<?php

include_once './controller/UserController.php';
include_once './controller/CategoryController.php';
include_once './controller/TopicController.php';
include_once './controller/PostController.php';


if (isset($_GET['action']) && isset($_GET['controller'])) {

    $controller =  new $_GET['controller'];
    $action=$_GET['action'];
    $controller->$action();

}

