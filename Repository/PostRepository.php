<?php
include_once 'BddConnection.php';


class PostRepository extends Bddconnection{


    public function addPost(Post $post)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO `post`(`postDate`, `content`, `id_user`, `id_topic`) VALUES (:posetDate,:content,:idUser,:idTopic)');

        $request->execute(array(
            'posetDate' => $post->getPostDate()->format('Y-m-d'),
            'content' => $post->getContent(),
            'idUser' => $post->getUser()->getId(),
            'idTopic' => $post->getTopic()->getId()
        ));

    }

    public function findAll(): array
    {

        $pdo = $this->getPdo();

        $request = $pdo->prepare('SELECT * FROM `post` WHERE 1');
        $request->execute();
        $post = $request->fetchAll(PDO::FETCH_ASSOC);

        return $post;

    }


    public function update(Post $post){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE `post` SET `postDate`=:postData,`content`=:content,`id_user`=:idUser,`id_topic`=:idTopic WHERE `id`=:id');

        $request->execute(array(
            'id' => $post->getId(),
            'postData' => $post->getPostDate()->format('Y-m-d'),
            'content' => $post->getContent(),
            'idUser' => $post->getUser()->getId(),
            'idTopic' => $post->getTopic()->getId(),

        ));
    }

    public function delete(int $idPost){
        $pdo = $this->getPdo();

        $request = $pdo->prepare('DELETE FROM `post` WHERE `id`=:id ');
        $request->execute(array(
            'id' => $idPost
        ));
    }
}