<?php

class Bddconnection{

    private PDO $pdo;

    public  function __construct(){
        $user = "root";
        $password = "";
        $dbname = "forum";

        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $password);
        } catch (PDOException $e) {
            throw $e;
        }

      
    }


    protected function getPdo(): PDO
    {
        return $this->pdo;
    }
}