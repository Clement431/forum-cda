<?php
include_once 'BddConnection.php';


class TopicRepository extends Bddconnection{


    public function addTopic(Topic $topic)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO `topic`( `title`, `id_user`, `id_category`) VALUES (:title,:idUser,:idCategory)');

        $request->execute(array(
            'title' => $topic->getTitle(),
            'idUser' =>$topic->getUser()->getId(),
            'idCategory' =>$topic->getCategory()->getId()

        ));
   
    }

    public function findAll(): array
    {

        $pdo = $this->getPdo();

        $request = $pdo->prepare('SELECT * FROM topic INNER JOIN category on topic.id_category = category.id INNER JOIN user on topic.id_user = user.id');
        $request->execute();
        $topic = $request->fetchAll(PDO::FETCH_ASSOC);

        return $topic;

    }

    public function findById(int $idTopic): Topic
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT `id`, `title`, `id_user`, `id_category` FROM `topic` WHERE `id`=:id');
        $request->execute(array("id"=> $idTopic));
        $dataTopic = $request->fetch(PDO::FETCH_ASSOC);

        $userRepository = new UserRepository();
        $categoryRepository = new CategoryRepository();


        $topic = new Topic();
        $topic->setId($dataTopic['id']);
        $topic->setTitle($dataTopic['title']);
        $topic->setUser($userRepository->findById($dataTopic['id_user']));
        $topic->setCategory($categoryRepository->findById($dataTopic['id_category']));

        return $topic;
    }

    public function update(Topic $topic){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE `topic` SET `title`=:title,`id_user`=:idUser,`id_category`=:idCategory WHERE `id`=:id');
        
        $request->execute(array(
            'id' => $topic->getId(),
            'title' => $topic->getTitle(),
            'idUser' => $topic->getUser()->getId(),
            'idCategory' =>$topic->getCategory()->getId()

        ));

       
    }

    public function delete(int $idTopic){
        $pdo = $this->getPdo();

        $request = $pdo->prepare('DELETE FROM `topic` WHERE `id`=:id ');
        $request->execute(array(
            'id' => $idTopic
        ));
    }
}