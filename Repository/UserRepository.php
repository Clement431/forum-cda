<?php
include_once 'BddConnection.php';

class UserRepository extends Bddconnection{


    public function addUser(User $user)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO `user`( `email`, `password`, `birthDate`) VALUES (:email,:password,:birthDate)');

        $request->execute(array(
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'birthDate' =>$user->getBirthDate()->format('Y-m-d')
        ));

    }

    public function findAll(): array
    {

        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT `id`, `email`, `password`, `birthDate` FROM `user` WHERE 1');
        $request->execute();
        $users = $request->fetchAll(PDO::FETCH_ASSOC);

        return $users;

    }

    public function findById(int $idUser): User
    {

        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT `id`, `email`, `password`, `birthDate` FROM `user` WHERE `id`=:id');
        $request->execute(array("id"=> $idUser));
        $dataUsers = $request->fetch(PDO::FETCH_ASSOC);

        $user = new User();
        $user->setId($dataUsers['id']);
        $user->setEmail($dataUsers['email']);
        $user->setPassword($dataUsers['password']);
        $user->setBirthDate(new \DateTime ($dataUsers['birthDate']));

        return $user;

    }

    public function update(User $user){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE `user` SET `email`=:email,`password`=:password,`birthDate`=:birthDate WHERE `id` =:id');

        $request->execute(array(
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'birthDate' =>$user->getBirthDate()->format('Y-m-d')
        ));
    }

    public function delete(int $idUser){
        $pdo = $this->getPdo();

        $request = $pdo->prepare('DELETE FROM `user` WHERE `id`=:id ');
        $request->execute(array(
            'id' => $idUser
        ));
    }
}