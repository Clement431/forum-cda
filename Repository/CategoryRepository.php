<?php
include_once 'BddConnection.php';

class CategoryRepository extends Bddconnection{


    public function addCategory(Category $category)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO `category`(`label`) VALUES (:label)');

        $request->execute(array(
            'label' => $category->getLabel(),

        ));

    }

    public function findAll(): array
    {

        $pdo = $this->getPdo();

        $request = $pdo->prepare('SELECT `id`, `label` FROM `category` WHERE 1');
        $request->execute();
        $category = $request->fetchAll(PDO::FETCH_ASSOC);

        return $category;

    }


    public function findById(int $idCategory): Category
    {

        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT `id`, `label` FROM `category` WHERE `id`=:id');
        $request->execute(array("id"=> $idCategory));
        $dataUsers = $request->fetch(PDO::FETCH_ASSOC);

        $category = new Category();
        $category->setId($dataUsers['id']);
        $category->setLabel($dataUsers['label']);

        return $category;

    }

    public function update(Category $category){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE `category` SET `label`=:label WHERE `id`=:id');

        $request->execute(array(
            'id' => $category->getId(),
            'label' => $category->getLabel(),
        ));
    }

    public function delete(int $idCategory){
        $pdo = $this->getPdo();

        $request = $pdo->prepare('DELETE FROM `category` WHERE `id`=:id ');
        $request->execute(array(
            'id' => $idCategory
        ));
    }
}